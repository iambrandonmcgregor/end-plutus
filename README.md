End Plutus
========
- - - -
We're creating an application to enable people to borrow money from family, friends, and anyone else who wishes to help them in their cause.
Traditionally, this would be done through a bank. After the recent collapse of the financial markets and increase of public awareness of corruption, many people would like another option. This is the opportunity that we wish to full. Rather than corporate banking institutions lending and making tremendous profits, why not put that interest into the pockets of your family and friends?
- - - - - - - - - - - - -
That's the goal.... now how are we going to get there?

With plenty of JavaScript and other great stuff!

+ [Backbone](http://www.backbonejs.org/)
+ [Marionette](http://marionettejs.com/)
+ [RequireJS](http://requirejs.org/)
+ [NodeJS](http://nodejs.org/)
+ [SailsJS](http://sailsjs.org/#!getStarted)
+ [WebSockets](https://developer.mozilla.org/en-US/docs/WebSockets)
+ [MySQL](http://www.mysql.com/)

And no small amount of tooling.

+ [GulpJS](http://gulpjs.com/)
+ [Sass](http://sass-lang.com/)
+ [EditorConfig](http://editorconfig.org/)
+ [Git](http://git-scm.com/)
+ [MySQL Workbench](https://dev.mysql.com/downloads/tools/)
+ [Node-Inspector](https://github.com/node-inspector/node-inspector)
+ [Bower](https://github.com/bower/bower)
+ [NPM](https://www.npmjs.org/)
- - - - - - - - - - - - -
##How do I Install it?
- - - -
OS Level Program's you'll need to start with.

+ [Git](http://git-scm.com/downloads)
+ [NodeJS](http://nodejs.org/)
+ [MySQL](http://dev.mysql.com/downloads/mysql/)
+ [Ruby](https://www.ruby-lang.org/en/downloads/)
+ [EditorConfig](http://editorconfig.org/)

Once you've got those installed, the remaining installs should be universal among operating systems. *You may need to prefix these commands with sudo, depending on your development environment.*


**Install the Sass ruby gem**
~~~~
gem install sass
~~~~
**Install SailsJS, GulpJS, Bower, and Node-Inspector**
~~~~
npm -g install sails gulp bower node-inspector
~~~~
**Get the code and get into it**
~~~~
cd projects_directory && git clone https://iambrandonmcgregor@bitbucket.org/iambrandonmcgregor/end-plutus.git && cd end-plutus
~~~~
**Install the application dependencies**
~~~~
npm install && bower install
~~~~
**Install the backend dependencies**
~~~~
cd backend && npm install
~~~~

###That's it! You're ready to rock n' roll!
- - - - 
##How do I run it?
- - - -

**LiveReload + Node Debugging + Server starting + Browser opening = fuck yeah!**
~~~~
gulp full
~~~~

**Or launch just the backend...**
~~~~
cd backend && sails lift
~~~~

**... or just the application**
~~~~
gulp
~~~~