var UsersController = {

	// GET -- Replaces 'login'
	find: function (req, res) {
		var email = req.param('email');
		var password = req.param('password');
		Users.findOne({email: email}).done(function resLogin(err, user){
			if (err) {
				res.send(500, {err:'DB Error'});
			} else {
				if (user){
					// will need to add a hasher once closer to deployment
					if (password === user.password) {
						req.session.user = user;
						req.session.save();
						res.send(user);
					} else {
						res.send(400, {err: 'Wrong password'});
					}
				} else {
						res.send(400, {err: 'User not found'});
				}
			}
		});
	},

	// POST -- Replaces 'signup'
	create: function (req, res) {
		var fullName = req.param('fullName');
		var email = req.param('email');
		var password = req.param('password');

		// *.find references the model not the actual table
		Users.findOne({email:email}).done(function userExistCheck(err, userCheck){
			if (err) {
				res.send(500, {err:'DB Error'});
			} else if (userCheck) {
				res.send(400, {err:'User already exists'});
			} else {
				Users.create({
					fullName: fullName,
					email: email,
					password: password
				}).done(function resCreateUser(err, user){
					// will need to add a hasher once closer to deployment
					if (err) {
						res.send(500, {err: 'DB Error'});
					} else {
						req.session.user = user;
						req.session.save();
						res.send(user);
					}
				});
			}
		});
	},

	// PUT
	update: function (req, res) {
		//handle logout
		if (req.param('logout')) {
			delete req.session.user;
			req.session.save();
			res.send(200, {id: null});
		}
		else
			res.send(500);
	},

	// DELETE
	destroy: function (req, res) {},

	connect: function (req, res) {
		res.jsonp(200, GetApplicationInitializationData(req));
	}
};

function GetApplicationInitializationData (req) {

	// Create a JSON payload to bootstrap the application with relavent data.
	var appInitData = {
		user: req.session.user
	};

	return appInitData;
}

module.exports = UsersController;
