var RSVP = require('rsvp');

var fundController = {

  find: function (req, res) {

    // reference the ID provided as a route parameter.
    var id = req.param('id');

    RSVP.all([GetFund(), GetContributions()])
    .then(function (FandC) {
      FandC[0].contributions = FandC[1];
      res.json(FandC[0]);
    })
    .catch(function (reason) {
      res.send(500, reason);
    });

    // Promise function definitions.
    // -----------------------------
    function GetFund () {
      return new RSVP.Promise(function (resolve, reject) {
        // Get the fund.
        Fund.findOneById(id)
        .done(function (err, fund) {
          if (err)
            reject(new Error(err));
          else
            resolve(fund);
        })
      });
    }
    function GetContributions () {
      return new RSVP.Promise(function (resolve, reject) {
        Contribution.find({fundID:id})
        .done(function (err, contributions) {
          if (err)
            reject(new Error(err));
          else
            resolve(contributions);
        });
      });
    }
  }
};

module.exports = fundController;
