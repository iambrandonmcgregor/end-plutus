/*
	Name: Fund
	Type: Model
	Description: This model describes the attributes found in a fund.
*/

var fund = {
	attributes: {
		ownerID: {
			type: 'integer',
			required: true
		},
		fundID: {
			type: 'string',
			required: false
		},
		fundName: {
			type: 'string',
			required: true
		},
		description: {
			type: 'text',
			required: false
		},
		fundingGoal: {
			type: 'float',
			required: true
		},
		fundraisingDeadline: {
			type: 'date',
			required: false
		},
		repaymentStart: {
			type: 'date',
			required: false
		},
		repaymentEndDate: {
			type: 'date',
			required: false
		},
    isSealed: {
      type: 'boolean'
    }
	},

  // Strip our createdAt & updatedAt fields before saving.
  beforeUpdate: function (valuesToUpdate, next) {
    delete valuesToUpdate.createdAt;
    delete valuesToUpdate.updatedAt;
    return next();
  }
};

module.exports = fund;
