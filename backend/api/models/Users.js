/**
 * Users
 *
 * @module      :: Model
 * @description :: Model to hold the user data
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

var users = {
	attributes: {
		fullName: {
			type: 'STRING',
			required: true},
		email: {
			type: 'email',
			required: true},
		password: {
			type: 'STRING',
			required: true}
	},

  // Strip our createdAt & updatedAt fields before saving.
  beforeUpdate: function (valuesToUpdate, next) {
    delete valuesToUpdate.createdAt;
    delete valuesToUpdate.updatedAt;
    return next();
  }
};

module.exports = users;
