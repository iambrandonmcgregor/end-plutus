/*
	Name: Contribution
	Type: Model
	Description: This model describes the attributes found in a fund contribution.
*/

var contribution = {

  // Define the model attributes.
	attributes: {
		fundID: {
			type: 'string',
			required: false
		},
    amount: {
      type: 'float',
      required: false
    },
		interestRate: {
			type: 'float',
			required: false
		},
    interestType: {
      type: 'string',
      required: false
    },
    repaymentStart: {
      type: 'date',
      required: false
    },
    repaymentEnd: {
      type: 'date',
      required: false
    },
    ownerID: {
      type: 'integer',
      required: false
    },
    isAccepted: {
      type: 'boolean',
      required: true
    }
	},

  // Ensure the associated fund exists before creating.
  beforeCreate: function (values, next) {
    Fund.findOneById(values.fundID)
    .done(function (err) {
      return next(err);
    });
  },

  // Strip our createdAt & updatedAt fields before saving.
  beforeUpdate: function (valuesToUpdate, next) {
    delete valuesToUpdate.createdAt;
    delete valuesToUpdate.updatedAt;
    return next();
  }

};

module.exports = contribution;
