/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

  // if the user is logged in, add their id to the request & proceed...
  if (req.session.user) {
    req.body.ownerID = req.session.user.id;
    return next();
  }

  // ...otherwise alert the user that they're forbidden.
  return res.forbidden('You must be logged in!');
};
