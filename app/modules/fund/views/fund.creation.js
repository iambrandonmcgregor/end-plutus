
'use strict';
define([
  'modules/mesopotamia/mesoRactive',
  'rv!../templates/fund.creation'
], function (MesoRactive, Template) {

  var FundCreationView = MesoRactive.extend({
    template: Template,

    // Class-level functions here.
    create: function (event) {

      // clear the error.
      this.data.error = null;

      // make the save attempt and handle the outcome.
      this.data.fund.save()
      .then(
        function (fund) {
          console.log('fund id: ' + fund.id);
        },
        function (error) {
          this.data.error = error;
        }
      );
    },

    init: function (options) {

      // call the base-class function.
      this._super(options);

      // Add instance-level stuff here, including linking to class-level functions.
      this.on({
        create: this.create
      });
    }

  });

  return FundCreationView;

});
