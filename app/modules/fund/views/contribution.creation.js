
"use strict";
define([
  'modules/mesopotamia/mesoRactive',
  'rv!../templates/contribution.creation',
  'numeral'
], function (MesoRactive, Template, Numeral) {

  var ContributionCreationView = MesoRactive.extend({
    template: Template,
    makeOffer: function (event) {
      this.data.contribution.save()
      .then(function (response) {
        console.log("You're contribution has been saved.");
        console.log(response);
        console.log("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
      });
    },

    computed: {
      interestRate: {
        get: function () {
          return (this.get('contribution.interestRate'))
            ? this.get('contribution.interestRate') * 100
            : null;
        },
        set: function (interestRate) {
          console.log("set interestRate");
          this.data.contribution.set('interestRate', (interestRate * .01));
        }
      }
    },

    init: function (options) {

      // call the base-class function.
      this._super(options);

      // Provide template-level access to the NumeralJS library.
      this.set('Numeral', Numeral);

      this.on({
        makeOffer: this.makeOffer
      })
    }

  });

  return ContributionCreationView;

});
