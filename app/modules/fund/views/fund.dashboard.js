
'use strict';
define([
  'underscore',
  'modules/mesopotamia/mesoRactive',
  'rv!../templates/fund.dashboard',
  'modules/user/user.module',
  'numeral',
  '../models/contribution.model',
  './contribution.creation'
], function (_, MesoRactive, Template, UserModule, Numeral, ContributionModel, ContributionCreationView) {

  var FundDashboardView = MesoRactive.extend({
    template: Template,
    // subviews: [
    //   {
    //     name: 'contributionEditor',
    //     view: ContributionCreationView,
    //     el: '.your-contribution-region'
    //   }
    // ],
    computed: {
      isFundOwner: '${user.id} === ${fund.ownerID}'
    },

    sealFund: function (event) {
      if (window.confirm('Are you sure you wish to seal/un-seal your fund?'))
        this.data.fund.toggleSeal()
        .then(function (fund) {
          console.log('Has your fund been sealed?.. ' + fund.get('isSealed'));
        });
    },

    viewContributionDetails: function (contributionId) {
      contributionId = parseInt(contributionId);
      this.set('expandedContribution', this.get('fund.contributions').findWhere({
        id: contributionId
      }));
    },

    init: function (options) {

      // call the base-class function.
      this._super(options);

      this.module = require('modules/fund/fund.module');

      // Attach the user model to the template's data.
      this.set('user', UserModule.user);

      // Provide template-level access to the NumeralJS library.
      this.set('Numeral', Numeral);

      // Bind instance-level events to class-level functions.
      this.on({
        sealFund: this.sealFund
      });

      // Add the contribution data to the Contribution Editor subview.
      this.on("subviewsInitialized", function () {
        _.findWhere(this.data.subviews, {name:'contributionEditor'})
        .view.set('contribution', this.module.getUserContribution());
      });

    }

  });

  return FundDashboardView;

});
