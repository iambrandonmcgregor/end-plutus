
define([
  'underscore',
  'backbone'
], function (_, Backbone) {

  var FundRouter = Backbone.Router.extend({

    initialize: function (controller) {
      this.route('create-fund', 'createFund', controller.createFund);
      this.route('create-fund/:fundId', 'editFund', controller.editFund);
      this.route('fund/:fundId(/contribution/:contributionId)', 'openFund', controller.openFund);
      this.route('contribution/:contributionId', 'openContribution', controller.openContribution);
    }
  });

  return FundRouter;

});
