
'use strict';
define([
  'app',
  'rsvp',
  './models/fund.model',
  './models/contribution.model',
  './fund.router',
  './views/fund.creation',
  './views/fund.dashboard',
  './views/contribution.creation',
  'modules/user/user.module'
], function ( App, RSVP, FundModel, ContributionModel, FundRouter, FundCreationView,
              FundDashboardView, ContributionCreationView, UserModule )
{

  var FundModule = {
    fund: new FundModel(),
    controller: {
      openFund: function (fundId, contributionId) {

        UpdateFundDataIfNecessary()
        .then(ShowViewIfNecessary)
        .then(ShowContributionDetailsIfNecessary);

        // --------------- - - - - - - - - - - - - - ---------------
        function UpdateFundDataIfNecessary () {

          return (FundModule.fund.get('id') != fundId)
            ? FundModule.fund.reset({id:fundId}).fetch()
            : new RSVP.Promise(function (resolve, reject) {
                resolve(FundModule.fund);
              });
        }

        // --------------- - - - - - - - - - - - - - ---------------
        function ShowViewIfNecessary (fund) {

          return (App.views.main instanceof FundDashboardView)
            ? App.views.main
            : App.show(new FundDashboardView({
                data: {fund:fund}
              }));
        }

        // --------------- - - - - - - - - - - - - - ---------------
        function ShowContributionDetailsIfNecessary (dashboardView) {
          if (contributionId !== undefined && contributionId !== null)
            dashboardView.viewContributionDetails(contributionId);
          return true;
        }

      },
      createFund: function () {
        FundModule.fund.reset({ id: null });
        App.show(new FundCreationView({
          data: { fund: FundModule.fund }
        }));
      },
      editFund: function (fundId) {

        // Reset the fund if necessary.
        if (FundModule.fund.get('id') !== fundId)
          FundModule.fund.reset({ id: fundId });

        // Ensure the fund is current from the server.
        FundModule.fund.fetch()
        .then(function (fund) {
          App.show(new FundCreationView({
            data: { fund: FundModule.fund }
          }));
        });

      },
      openContribution: function (contributionId) {
        // Reset the fund and populate the specified contribution...
        FundModule.fund.reset()
        .get('contributions').add({id: contributionId}).fetch()

        // ...then populate the fund based on the parent of the contribution...
        .then(function Success (contribution) {
          return FundModule.fund.set('id', contribution.get('fundID')).fetch();
        }, function Failure (error) {
          console.log('Error loading the specified contribution...');
          console.log(error);
        })

        // ...then load the contribution view.
        .then(function (fund) {
          App.show(new ContributionCreationView({
            data: {
              contribution: fund.get('contributions').findWhere({ id: parseInt(contributionId) })
            }
          }));
        });
      }
    }
  };

  // Add the router, passing it the controller object.
  FundModule.router = new FundRouter(FundModule.controller);

  // Add a function to grab the current user's contribution.
  FundModule.getUserContribution = function GetUserContribution() {
    return FundModule.fund.get('contributions').findWhere({ownerID:UserModule.user.get('id')})
      || new ContributionModel({fundID:FundModule.fund.get('id')});
  };

  return FundModule;
});
