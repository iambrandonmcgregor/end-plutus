
define([
  'modules/mesopotamia/mesoModel'
], function ContributionConstructor(MesoModel) {

  var ContributionModel = MesoModel.extend({

    urlRoot: '/contribution',

    defaults: function () {
      return {
        fundID: null,
        amount: null,
        interestRate: null,
        interestType: null,
        repaymentStart: null,
        repaymentEnd: null,
        isAccepted: false
      };
    },

    initialize: function (attributes) {

      // parse string dates into JS Date objects.
      this.set(this.parse(attributes), {silent:true});

      // Setup computed property listeners.
      this.on('change:interestRate', function (model, value, options) {
        this.updateTotalInterest();
      });
      this.on('change:amount', function (model, value, options) {
        this.updateTotalInterest();
      });
      this.on('change:repaymentStart', function (model, value, options) {
        this.updateTotalInterest();
        this.updateNumberOfPayments();
      });
      this.on('change:repaymentEnd', function (model, value, options) {
        this.updateTotalInterest();
        this.updateNumberOfPayments();
      });
      this.on('change:totalInterest', function (model, value, options) {
        this.updateTotalCost();
      });
      this.on('change:totalCost', function (model, value, options) {
        this.updateCostPerDollar();
        this.updatePaymentAmount();
      });
      this.on('change:numberOfPayments', function (model, value, options) {
        this.updatePaymentAmount();
      });

      // kick it off by computed all properties.
      this.updateAll();
    },

    parse: function (attributes, options) {
      attributes = MesoModel.prototype.parse.apply(this, arguments);
      this.parseDates(attributes, ['repaymentStart', 'repaymentEnd']);
      return attributes;
    },


    // Computed Properties - Generator Functions.
    // -------------------------------------------------------------------------
    updateAll: function () {
      this.updateTotalInterest();
      this.updateNumberOfPayments();
    },

    updateTotalInterest: function () {
      this.set('totalInterest', GetContinuousCompoundInterest(
        this.get('amount'),
        this.get('interestRate'),
        this.getNumberOfYearsBetweenDates(
          this.get('repaymentStart'),
          this.get('repaymentEnd')
        )
      ));
    },

    updateTotalCost: function () {
      this.set(
        'totalCost',
        (parseFloat(this.get('totalInterest')) + parseFloat(this.get('amount')))
      );
    },

    updateCostPerDollar: function () {
      this.set(
        'costPerDollar',
        (this.get('totalCost') / this.get('amount'))
      );
    },

    updateNumberOfPayments: function () {
      this.set('numberOfPayments', this.getNumberOfPayments(
        this.get('repaymentStart'), this.get('repaymentEnd')
      ));
    },

    updatePaymentAmount: function () {
      this.set(
        'paymentAmount',
        (this.get('totalCost') / this.get('numberOfPayments'))
      );
    }

  });


  // Private Functions.
  // ---------------------------------------------------------------------------
  function GetContinuousCompoundInterest (principle, rate, time) {

    var finalValue = (principle * Math.pow(Math.E, (rate*time)));

    return (finalValue - principle);
  }

  return ContributionModel;
});
