
define([
  'underscore',
  'backbone',
  'modules/mesopotamia/mesoModel',
  'modules/fund/models/contribution.model'
], function (_, Backbone, MesoModel, ContributionModel) {

  var FundModel = MesoModel.extend({


    // Backbone Model Overrides & Settings.
    // -------------------------------------------------------------------------

    urlRoot: '/fund',

    defaults: function () {
      return {
        fundName: null,
        fundingGoal: null,
        description: null,
        fundraisingDeadline: null,
        repaymentStart: null,
        contributions: (this.get('contributions')
          || new Backbone.Collection([],{model:ContributionModel}))
      };
    },

    initialize: function () {
      var contributions = this.get('contributions');

      // Re-compute all properties when a contribution is added or removed.
      this.listenTo(contributions, 'add', this.updateAll);
      this.listenTo(contributions, 'remove', this.updateAll);
      this.listenTo(contributions, 'reset', this.updateAll);

      // Listen for specific contribution-level property changes.
      this.listenTo(contributions, 'change:totalCost', this.updateTotalCost);
      this.listenTo(contributions, 'change:interestRate', this.updateAverageInterestRate);
      this.listenTo(contributions, 'change:amount', this.updateFundedAmount);

      // Listen for fund-level changes.
      this.on('change:fundedAmount change:totalCost', this.updateCostPerDollar);
    },

    parse: function (response, options) {

      // use the default Backbone.Model parse function.
      var parsed = MesoModel.prototype.parse.apply(this, arguments);

      // update the contributions.
      this.get('contributions').set(parsed.contributions, {remove:false});

      // delete the contributions from the parsed return.
      delete parsed.contributions;

      // parse dates.
      this.parseDates(parsed, ['fundraisingDeadline', 'repaymentStart']);

      return parsed;
    },


    // Custom Model Functions.
    // -------------------------------------------------------------------------
    reset: function (newAttributes) {

      // broadcast the "destroy" event.
      this.trigger("destroy", this);

      // ensure a 'newAttributes' object is present.
      newAttributes = _.isObject(newAttributes) ? newAttributes : {};

      // remove all attributes from the model, other than contributions.
      _.each(this.attributes, function (value, key, list) {
        if (key !== 'contributions') delete list.key;
      });

      // clear the contributions.
      this.get('contributions').reset();

      // set all specified 'newAttributes' plus any model defaults.
      this.set(_.defaults(newAttributes, this.defaults()));

      return this;
    },

    toggleSeal: function () {
      this.set('isSealed', !this.get('isSealed'));
      return this.save();
    },


    // Computed Properties - Generator Functions.
    // -------------------------------------------------------------------------
    updateAll: _.throttle(function () {
      this.updateFundedAmount();
      this.updateTotalCost();
      this.updateCostPerDollar();
      this.updateAverageInterestRate();
    }, 400),

    updateTotalCost: function () {
      this.set(
        'totalCost',
        this.get('contributions').reduce(function (memo, contribution) {
          return memo + parseFloat(contribution.get('totalCost'));
        }, 0)
      );
    },

    updateFundedAmount: function () {
      this.set(
        'fundedAmount',
        this.get('contributions').reduce(function (memo, contribution) {
          return memo + parseFloat(contribution.get('amount'));
        }, 0)
      );
    },

    updateCostPerDollar: function () {
      this.set(
        'costPerDollar',
        (this.get('totalCost') / this.get('fundedAmount'))
      );
    },

    updateAverageInterestRate: function () {
      var contributions = this.get('contributions');
      if (contributions.length <= 0) return this;

      this.set(
        'averageInterestRate',
        (
          contributions.reduce(function (memo, contribution) {
            return memo + contribution.get('interestRate');
          }, 0) / contributions.length
        )
      )
    }

  });

  return FundModel;
});
