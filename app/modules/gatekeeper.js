
'use strict';
define([
  'jquery',
  'backbone',
  'rsvp',
  'sailsIO',
  'modules/user/user.module'
], function GatekeeperFactory ($, Backbone, RSVP, SailsIO, UserModule) {

  // Private Gatekeeper properties.
  // ----------------------------------------------------------------------------------------------|
  var connectionAttemptPause = 4000;


  // Create our Gatekeeper Module. It's properties will be publicly accessible.
  // ----------------------------------------------------------------------------------------------|
  var Gatekeeper = {
    promise: null,
    socket: null,
    serverAddress: 'http://localhost:1337'
  };


  // Setup a main promise chain to ensure CRUD calls happen syncronously.
  // Start the chain off by establishing the socket connection with the backend server.
  // ----------------------------------------------------------------------------------------------|
  Gatekeeper.promise = new RSVP.Promise(function StartSocketConnection (resolve, reject) {
    var attempts = 0,
        timeout;

    // shake hands with the socker server via standard HTTP (JSONP).
    (function ShakeHandsWithTheBackendServer () {
      $.ajax({
        type: 'GET',
        url: Gatekeeper.serverAddress+'/connect',
        dataType: 'jsonp',
        success: function HandleSuccesfulHandshake (sessionData) {
          Gatekeeper.socket = SailsIO.connect(Gatekeeper.serverAddress);
          resolve(sessionData);
          UserModule.user.set(sessionData.user);
        },
        error: function HandleFailedHandshake (jqHXR, textStatus, textErrorThrown) {
          throw new Error(textErrorThrown);
          timeout = window.setTimeout(ShakeHandsWithTheBackendServer, connectionAttemptPause);
        }
      });
    })();
  });


  // Socketize Backbone's 'sync' function.
  // ----------------------------------------------------------------------------------------------|
  Backbone.sync = function (method, model, options) {

    // Attach this CRUD call to the main Gatekeeper promise chain and return the refreshed promise.
    return Gatekeeper.promise.then(function CRUDNext () {
      return new RSVP.Promise(function CRUD (resolve, reject) {
        // Execute the HTTP CRUD call via the Gatekeeper's socket.
        Gatekeeper.socket[GetHttpMethodName(method)](
          model.url(),
          model.toJSON(),
          function HandleCRUDResponse (response) {
            // If the server says there's an error...
            if (response.err || response.error || response.status >= 300) {
              // Call default Backbone error behavior.
              options.error(response);
              // Reject the promise for this call, providing the error as then 'then' parameter.
              reject(response);
            }
            // ...otherwise, it must be successful.
            else {
              // Call default Backbone success behavior.
              options.success(response);
              // Resolve the promise for this call, providing the model as the 'then' parameter.
              resolve(model);
            }
          }
        );
      });
    });
  }


  // Private functions.
  // ----------------------------------------------------------------------------------------------|
  function GetHttpMethodName (BackboneSyncMethod) {
    switch (BackboneSyncMethod) {
      case 'create':
        return 'post';
      case 'read':
        return 'get';
      case 'update':
        return 'put';
      case 'delete':
        return 'delete';
      default:
        return false;
    }
  }


  return Gatekeeper;

})
