
define([
  'modules/mesopotamia/mesoRactive',
  'rv!../templates/user.preview.view',
], function (MesoRactive, Template) {

  var UserPreviewView = MesoRactive.extend({
    template: Template,
    login: function (event) {
      this.module.promptLogin();
    },
    logout: function (event) {
      this.data.user.logout();
    },

    // Instance-Level Actions.
    // --------------------------------------------------------------------------------------------|
    init: function (options) {

      // call the base-class function.
      this._super(options);

      this.module = options.module;
      this.on({
        login: this.login,
        logout: this.logout
      });
    }
  });

  return UserPreviewView;

});
