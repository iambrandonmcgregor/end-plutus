

define([
  'jquery',
  'modules/mesopotamia/mesoRactive',
  'rv!../templates/login.modal'
], function ($, MesoRactive, Template) {

  var LoginModalView = MesoRactive.extend({
    template: Template,

    login: function (event) {
      this.data.user.fetch();
    },

    signup: function (event) {
      alert('LoginModalView.signup has not yet been implemented.');
    },

    attachDOMListeners: function () {
      // cache references to the current scope and the DOM.
      var self = this,
          $el = $(this.el);

      // Destroy this view when the modal is closed.
      $el.on('hidden.bs.modal', function RazeView () {
        // remove this listener.
        $el.off('hidden.bs.modal', RazeView);
        // clear the password field.
        self.data.user.unset('password', null);
      });

      // When the User ID changes (aka signing or signout), close the modal.
      this.observe('user.id', function (newValue, oldValue, keypath) {
        if (newValue)
          $el.modal('hide');
      });
    },

    init: function (options) {

      // call the base-class function.
      this._super(options);

      // Respond to instance-level events.
      this.on({
        login: this.login,
        signup: this.signup,
        domInsertion: this.attachDOMListeners
      });
    }
  });

  return LoginModalView;

});
