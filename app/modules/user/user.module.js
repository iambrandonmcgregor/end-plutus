
'use strict';
define([
  'app',
  'modules/user/models/user.model',
  './views/user.preview.view',
  './views/login.modal.view'
], function (App, UserModel, UserPreviewView, LoginModalView) {

  var UserModule = {
    user: new UserModel(),
    promptLogin: function PromptLogin () {
      App.showModal(new LoginModalView({
        data: { user: UserModule.user }
      }));
    }
  };

  // Initialize views.
  App.show(new UserPreviewView({
    data: {
      user: UserModule.user
    },
    module: UserModule
  }), 'userPreview');

  return UserModule;
});
