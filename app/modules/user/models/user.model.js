
"use strict";
define([
  'jquery',
  'underscore',
  'backbone'
], function ($, _, Backbone) {

  var UserModel = Backbone.Model.extend({

    // Default Model overrides.
    // --------------------------------------------------------------------------------------------|
    urlRoot: '/user',

    defaults: {
      id: null,
      username: '',
      email: '',
      firstName: '',
      lastName: '',
      fullName: ''
    },

    initialize: function (options) {
      // Update the 'fullName' when the first or last name change.
      this.on('change:firstName', this.updateFullName);
      this.on('change:lastName', this.updateFullName);
    },

    logout: function () {
      if (!this.isNew()) {
        var self = this;
        this.set('logout', true);
        this.save().then(function () {
          return self.clear();
        }, function () {
          console.warn('There was an error saving the User model to the server.');
          return false;
        });
      }
      else {
        return false;
      }
    },

    // Computed property functions.
    // --------------------------------------------------------------------------------------------|
    updateFullName: function (model, value, options) {
      model.set('fullName', (model.get('fistName') + model.get('lastName')));
    }

  });

  return UserModel;
});
