
define([
  'backbone',
  'underscore',
  'moment'
], function (Backbone, _, Moment) {

  /*
    This is where all models began... it's got important things to build off of.
  */

  var MesoModel = Backbone.Model.extend({

    // Function to convert string dates (from server) to JS Date objects.
    parseDates: function (attributesObj, attributesToParse) {

      // exit execution if there's no attributesObj.
      if (!attributesObj)
        return false;

      // if specific attributes were specified, loop through em...
      if (_.isArray(attributesToParse)) {
        for (var value, i=0, l=attributesToParse.length; i<l; i++) {
          v = attributesObj[attributesToParse[i]];
          if (_.isString(v))
            attributesObj[attributesToParse[i]] = v.substring(0, v.indexOf('T'));
        }
      }
      // ...otherwise, check for 'Date' attributes manually.
      else {
        _.each(attributesObj, function (v, key, list) {
          if (key.indexOf('Date') !== -1)
            attributesObj[key] = v.substring(0, v.indexOf('T'));
        });
      }

      // Even though the attributesObj is a reference so it's already modified,
      // return it anyways. For fun.
      return attributesObj;
    },

    // Function to get the number of days between two string dates.
    getNumberOfYearsBetweenDates: function (start, end) {

      // Convert dates to MomentJS objects.
      start = Moment(start);
      end = Moment(end);

      return end.diff(start, 'years', true);
    },

    getNumberOfPayments: function (start, end) {

      // Convert dates to MomentJS objects.
      start = Moment(start);
      end = Moment(end);

      return end.diff(start, 'months', false) || '?';
    }

  });

  return MesoModel;

});
