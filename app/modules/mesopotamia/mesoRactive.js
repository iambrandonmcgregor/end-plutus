
'use strict';
define([
  'underscore',
  'ractive'
], function (_, Ractive) {

  var MesoRactive = Ractive.extend({
    adapt: ['Backbone'],
    init: function (options) {

      // Upon DOM Insertion, add any/all sub-views.
      this.on('domInsertion', function ($el) {

        // break execution if there are no subviews.
        if (!_.isArray(this.subviews) || this.subviews.length <= 0)
          return false;

        // create a collection of view instances.
        this.data.subviews = [];

        // initialize all subviews.
        for (var i=0, l=this.subviews.length, subview; i<l; i++) {
          subview = _.clone(this.subviews[i]);
          subview.el = $el.find(subview.el);
          subview.view = new subview.view({
            el: subview.el,
            data: subview.data
          });
          this.data.subviews.push(subview);
        }

        // alert the view that the subviews have all been added.
        this.fire("subviewsInitialized");

        // teardown all subviews when this view is torndown.
        this.on('teardown', function () {
          for (var i=0, l=this.data.subviews.length, subview; i<l; i++)
            this.data.subviews[i].view.teardown();
        });

      });

    }
  });

  return MesoRactive;

});
