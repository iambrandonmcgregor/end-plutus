
'use strict';
define([
  'requestAnimationFrame',
  'backbone'
], function (rAF, Backbone) {

  // polyfill capture to ensure we get an accurate screensize regardless of the current browser.
  var w = window,
      d = document,
      e = d.documentElement,
      g = d.getElementsByTagName('body')[0];

  // create a new Backbone model instance to represent the screen. This will provide events.
  var Screen = new Backbone.Model({
    width: w.innerWidth || e.clientWidth || g.clientWidth,
    height: w.innerHeight|| e.clientHeight|| g.clientHeight
  });


  (function StartOptimizedResizeEvent () {

    var debouncing = false;

    window.addEventListener('resize', function ResizeEvent () {
      if (!debouncing) {
        debouncing = true;
        window.requestAnimationFrame(function ProcessResize () {
          var dimensions = {
            width: w.innerWidth || e.clientWidth || g.clientWidth,
            height: w.innerHeight || e.clientHeight || g.clientHeight
          };
          Screen.set(dimensions);
          Screen.trigger('resize', dimensions);
          debouncing = false;
        });
      }
    });

  })();

  return Screen;

});
