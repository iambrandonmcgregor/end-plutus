
define([
  'modules/mesopotamia/mesoRactive',
  'rv!../templates/welcome'
], function (MesoRactive, Template) {

  var WelcomeView = MesoRactive.extend({
    template: Template
  });

  return WelcomeView;

});
