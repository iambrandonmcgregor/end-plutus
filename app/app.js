
"use strict";
define([
  'jquery',
  'backbone',
  'router',
  'ractive',
  'rsvp'
],
function ($, Backbone, Router, Ractive, RSVP) {

  var App = {
    ui: {
      userPreview: $('#UserPreview'),
      main: $('#ApplicationBody'),
      modal: $('#AppModal')
    },
    views: {},
    show: function (view, where) {

      if (!where)
        where = 'main';

      if (App.views[where] instanceof Ractive)
        return App.views[where].teardown().then(ShowNewView);
      else
        return new RSVP.Promise(function (resolve, reject) {
          resolve(ShowNewView());
        })

      function ShowNewView () {
        delete App.views[where];
        App.views[where] = view;
        App.views[where].insert(App.ui[where]);
        App.views[where].fire('domInsertion', App.ui[where]);
        return App.views[where];
      }
    },
    showModal: function (view) {
      // Hide the current modal if one exists.
      if (App.views.modal) {
        // Attach the modal-is-hidden listener.
        App.ui.modal.on('hidden.bs.modal', DisplayModal);
        // trigger the modal to hide.
        App.ui.modal.modal('hide');
      }
      else {
        DisplayModal();
      }


      function DisplayModal () {
        // remove the current modal hide listener.
        App.ui.modal.off('hidden.bs.modal', DisplayModal);

        // Cache a reference to the view.
        App.views.modal = view;
        // Attach the view to the DOM.
        App.views.modal.insert(App.ui.modal);
        // Alert the view that it's now in the DOM.
        App.views.modal.fire('domInsertion');
        // Show the modal (Twitter Bootstrap Function).
        App.ui.modal.modal();
        // Listen for when the view is closed.
        App.ui.modal.on('hidden.bs.modal', function RazeView () {
          console.log('The App module heard the modal close.');
          App.views.modal.teardown().then(function () {
            delete App.views.modal;
          });
          App.ui.modal.off('hidden.bs.modal', RazeView);
        });
      }
    }
  };

  // Attach the router.
  App.router = new Router({App:App});

  return App;
});
