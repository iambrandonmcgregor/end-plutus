
// Configure require.js
require.config({
  paths : {
    'text': 'bower_components/requirejs-text/text',
    'tpl': 'bower_components/requirejs-tpl/tpl',
    'backbone': 'bower_components/backbone/backbone',
    'underscore': 'bower_components/underscore/underscore',
    'jquery': 'bower_components/jquery/dist/jquery',
    'socketIO': 'bower_components/socket.io-client/dist/socket.io.min',
    'sailsIO': 'bower_components/bower-sails.io/sails.io',
    'bootstrap': 'bower_components/bootstrap/dist/js/bootstrap',
    'rsvp': 'bower_components/rsvp/rsvp.amd',
    'moment': 'bower_components/momentjs/moment',
    'numeral': 'bower_components/numeral/numeral',
    'amd-loader': 'bower_components/requirejs-ractive/amd-loader',
    'rv': 'bower_components/requirejs-ractive/rv',
    'rvc': 'bower_components/requirejs-ractive/rvc',
    'ractive': 'bower_components/ractive/ractive',
    'ractiveBackbone': 'bower_components/ractive-backbone/ractive-adaptors-backbone',
    'ractiveTap': 'bower_components/ractive-events-tap/ractive-events-tap',
    'requestAnimationFrame': 'bower_components/requestAnimationFrame/app/requestAnimationFrame',
    'paths': 'bower_components/paths-js/dist/amd'
  },
  shim : {
    jquery : {
      exports : 'jQuery'
    },
    underscore : {
      exports : '_'
    },
    backbone : {
      deps : ['jquery', 'underscore'],
      exports : 'Backbone'
    },
    sailsIO : {
      deps: ['socketIO'],
      exports: 'io'
    },
    bootstrap : ['jquery']
  }
});

  // Initialize the application.
require([
  'jquery',
  'app',
  'modules/fund/fund.module',
  'modules/gatekeeper',
  'modules/user/user.module',
  'bootstrap',
  'ractiveBackbone',
  'ractiveTap'
], function ($, app) {
  console.log('Application has been initialized.');
  Backbone.history.start();
})
