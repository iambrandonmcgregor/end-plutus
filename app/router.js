
define([
  'backbone',
  'modules/welcome/views/welcome.view'
], function (Backbone, WelcomeView) {

  var App = null;

  var Router = Backbone.Router.extend({

    initialize : function (options) {
      App = options.App;
    },

    // Most routes will be handled by separate module-level routers.
    routes : { '*path' : 'defaultRoute' },

    // Route Functions.
    // --------------------------------------------------------------------------------------------|
    defaultRoute : function () {
      console.log("Router loaded default (blank) route.");

      App.show(new WelcomeView());
    }
  });

  return Router;
});
