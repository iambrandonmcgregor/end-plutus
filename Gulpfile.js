
// Include all of our dependencies.
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    lr = require('tiny-lr'),
    liveReloadServer = lr(),
    open = require('open'),
    express = require('express'),
    exec = require('child_process').exec;


// Configure some project settings.
var settings = {
  serverRoot: (__dirname + '/app')
};


// Some options for your development environment.
var developmentEnvironment = {
  serverPortNumber: 8068,
  liveReloadPortNumber: 35729
};


// Define the task for CSS/Sass compilation.
gulp.task('styles', function () {
  return gulp.src(settings.serverRoot+'/styles/main.scss')
    .pipe(sass({ style: 'expanded' }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest(settings.serverRoot+'/styles'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest(settings.serverRoot+'/styles'))
    .pipe(livereload(liveReloadServer));
});


// Define the task for javascripts.
gulp.task('scripts', function () {
  return gulp.src([settings.serverRoot+'/*.js', settings.serverRoot+'/modules/**/*.js'])
    .pipe(livereload(liveReloadServer));
});


// Define the task to clean out the file structure.
gulp.task('clean', function () {
  return gulp.src([settings.serverRoot+'styles/**/*.css'], { read:false })
    .pipe(clean());
});


// Define the task to watch the file structure (live-reload).
gulp.task('watch', function () {
  liveReloadServer.listen(developmentEnvironment.liveReloadPortNumber, function (error) {
    // alert the user of errors and don't compile.
    if (error)
      return console.log(error);

    // watch for .scss changes.
    gulp.watch([settings.serverRoot+'/styles/**/*.scss', settings.serverRoot+'/modules/**/styles/*.scss'], ['styles']);
    // watch for .js & .html-template changes.
    gulp.watch([settings.serverRoot+'/*.js', settings.serverRoot+'/modules/**/*.js', settings.serverRoot+'/modules/**/*.html'], ['scripts']);
    // watch for root index.html changes.
    gulp.watch(settings.serverRoot+'/*.html', function (event) {
      gulp.src(event.path, { read:false }).pipe(livereload(liveReloadServer));
    });
  });
});


// Define the task to launch the SailsJS API with debugging.
gulp.task('api-debug', function launchAPI () {
  //var sails = exec('cd backend && sails lift', function (error, stdout, stderr) {
  	var sails = exec('cd backend && node-debug app.js', function (error, stdout, stderr) {
  	console.log('sails-out: ' + stdout);
  	console.log('sails-err: ' + stderr);
  	if (error) {
  		console.log('execution error: ' + error);
  	}
  });
});

// Define the task for launching the SailsJS API.
gulp.task('api', function launchAPI () {
  	var sails = exec('cd backend && sails lift', function (error, stdout, stderr) {
  	console.log('sails-out: ' + stdout);
  	console.log('sails-err: ' + stderr);
  	if (error) {
  		console.log('execution error: ' + error);
  	}
  });
});


// Define the default task. This runs with the 'gulp' command.
gulp.task('default', ['clean', 'styles', 'scripts'], function () {

  // Start the ExpressJS server to host the static app files.
  (function startExpress () {
    var express = require('express');
    var app = express();
    app.use(require('connect-livereload')());
    app.use('/', express.static(settings.serverRoot));
    app.listen(developmentEnvironment.serverPortNumber);
  })();

  // Start the gulp watch task.
  gulp.start('watch');

  // Launch the web browser to open our freshly launched ExpressJS server.
  open('http://localhost:' + developmentEnvironment.serverPortNumber);
});

// Define a task that includes launching the backend.
gulp.task('full', ['api'], function () {
	gulp.start('default');
});

// Define a task for launching the entire solution with debugging.
gulp.task('debug', ['api-debug'], function () {
	gulp.start('default');
});

